﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Data;
using System.Reflection;


namespace movable
{
    class CLI
    {
        /// <summary>
        /// Constructer for the class, adds the commands to the commandDict and the helpDict
        /// </summary>
        public CLI()
        {
            Tuple<Func<bool>, string, FunctionParameters.Parameters, int, string> M =
                new Tuple<Func<bool>, string,FunctionParameters.Parameters, int, string>(Functions.m, "moves stuff", FunctionParameters.Parameters.Yes, 2, "source destination");
            Tuple<Func<bool>, string, FunctionParameters.Parameters, int, string> HELPALL =
                new Tuple<Func<bool>, string, FunctionParameters.Parameters, int, string>(Functions.helpall, "Displays the general help", FunctionParameters.Parameters.No, 0, "");
            Tuple<Func<bool>, string, FunctionParameters.Parameters, int, string> QUIT =
                new Tuple<Func<bool>, string, FunctionParameters.Parameters, int, string>(Functions.quit, "Stops the program", FunctionParameters.Parameters.No, 0, "");
            Tuple<Func<bool>, string, FunctionParameters.Parameters, int, string> BACKUP =
                new Tuple<Func<bool>, string, FunctionParameters.Parameters, int, string >(Functions.backup, "Backups stuff to AWS glacier", FunctionParameters.Parameters.Yes, 1, "path");
            Tuple<Func<bool>, string, FunctionParameters.Parameters, int, string> HELP =
                new Tuple<Func<bool>, string, FunctionParameters.Parameters, int, string>(Functions.help, "Displays specific help", FunctionParameters.Parameters.Yes, 1, "function");
            Tuple<Func<bool>, string, FunctionParameters.Parameters, int, string> FROMBASE64 =
                new Tuple<Func<bool>, string, FunctionParameters.Parameters, int, string>(Functions.fromBase64, "Converts a base64 string to a readable string", FunctionParameters.Parameters.Yes, 1, "base64");
            Tuple<Func<bool>, string, FunctionParameters.Parameters, int, string> TOBASE64 =
                new Tuple<Func<bool>, string, FunctionParameters.Parameters, int, string>(Functions.toBase64, "Converts a string to a base64 string", FunctionParameters.Parameters.Yes, 1, "string");
            Tuple<Func<bool>, string, FunctionParameters.Parameters, int, string> PARSEGLADYS =
                new Tuple<Func<bool>, string, FunctionParameters.Parameters, int, string>(Functions.parseGladysLog, "Parses gladys log file", FunctionParameters.Parameters.Yes, 2, "path printerrors");
            
        

            Dictionaries.FunctionDict.Add("m", M);
            Dictionaries.FunctionDict.Add("helpall", HELPALL);
            Dictionaries.FunctionDict.Add("quit", QUIT);
            Dictionaries.FunctionDict.Add("backup", BACKUP);
            Dictionaries.FunctionDict.Add("help", HELP);
            Dictionaries.FunctionDict.Add("frombase64", FROMBASE64);
            Dictionaries.FunctionDict.Add("tobase64", TOBASE64);
            Dictionaries.FunctionDict.Add("parsegladys", PARSEGLADYS);
        }

        /// <summary>
        /// Parses the user's entry, and triggers the command.
        /// </summary>
        /// <param name="entry"> Console.ReadLine();  </param>
        public void parseUserEntry(string entry)
        {
            List<string> commands = new List<string>();
            foreach(string str in entry.Split(' '))
            {
                commands.Add(str);
            }
            if(Dictionaries.FunctionDict.ContainsKey(commands.ElementAt(0)))
            {
                string func = commands.ElementAt(0);
                if (!new FunctionAPI().functionNeedsArgs(Dictionaries.FunctionDict[commands.ElementAt(0)]))
                {
                    Dictionaries.FunctionDict[commands.ElementAt(0)].Item1();
                }
                else
                {
                    if (entry.Split(' ').Length > 1)
                    {
                        this.flushParams();
                        var res = from x in commands where x != commands.ElementAt(0) && x != "help" select x;
                        if(res.Count().Equals(Dictionaries.FunctionDict[commands.ElementAt(0)].Item4))
                        {
                            foreach (var v in res)
                            {
                                Functions.args.Add(v);
                            }
                            Dictionaries.FunctionDict[commands.ElementAt(0)].Item1();
                        }
                        else if (res.Count() > Dictionaries.FunctionDict[commands.ElementAt(0)].Item4)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Too many arguments, the " + Dictionaries.FunctionDict[commands.ElementAt(0)].Item4 + " first ones will be passed to the function");
                            for (int i = 0; i < Dictionaries.FunctionDict[commands.ElementAt(0)].Item4; i++)
                            {
                                Functions.args.Add(res.ElementAt(i));
                            }
                            Dictionaries.FunctionDict[commands.ElementAt(0)].Item1();
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Too few arguments, please consult the help of the function using 'help " + commands.ElementAt(0) + "'.");
                        }
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("The function needs parameters.\nConsult the help of the function using 'help " + commands.ElementAt(0) +"'.");
                    }
                }
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(commands.ElementAt(0) + " : Unknown command. Type help.");
                foreach(string str in new AI(entry).getCloseCommands())
                {
                    Console.WriteLine("Did you mean : " + str + " ?");
                }
            }
        }

        private void flushParams()
        {
            Functions.args.Clear();
        }
    }
}
