﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Reflection.Emit;

namespace movable
{
    class AI : IDisposable // gotta be called like new AI(stuff).getCloseCommands();
    {
        private string _wrongCommand;
        private Dictionary<string, Tuple<Func<bool>, string, FunctionParameters.Parameters, int, string>> _dict;

        public AI (string wrongCommand)
        {
            _wrongCommand = wrongCommand;
            _dict = Dictionaries.FunctionDict;
        }

        public List<string> getCloseCommands()
        {
            List<string> closeCommands = new List<string>();

            foreach(KeyValuePair<string, Tuple<Func<bool>, string, FunctionParameters.Parameters, int, string>> pair in _dict)
            {
                if(_wrongCommand.Length > 2)
                {
                    if (pair.Key.Contains(_wrongCommand))
                    {
                        closeCommands.Add(pair.Key);
                    }
                }
                
               if(getNumberOfSharedLetters(_wrongCommand, pair.Key) > pair.Key.Length / 2)
                {
                    if(!closeCommands.Contains(pair.Key))
                    {
                        closeCommands.Add(pair.Key);
                    }
                }
            }
            return closeCommands;
        }

        public int getNumberOfSharedLetters(string wrong, string gud)
        {
            int numbr = 0;
            for (int i = 0; i < wrong.Length; i++ )
            {
                if(gud.Contains(wrong[i]))
                {
                    numbr++;
                    gud.Remove(gud.IndexOf(wrong[i]), 1);
                }
            }
            return numbr;
        }

        public void Dispose()
        {
            this._dict = null;
            this._wrongCommand = null;
        }
    }
}
