﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Data;
using System.Reflection;


namespace movable
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Movable.");
            Console.WriteLine(Environment.NewLine);
            CLI cli = new CLI();
            watcher watcher = new watcher();

            while(true)
            {
                Console.ForegroundColor = ConsoleColor.Gray;
                string entry = Console.ReadLine();
                Console.WriteLine(Environment.NewLine);
                cli.parseUserEntry(entry.Replace("  ", "").TrimEnd());
                Console.WriteLine(Environment.NewLine);
            }
        }
    }
}