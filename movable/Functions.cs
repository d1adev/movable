﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using Newtonsoft.Json;
using RiotSharp;

namespace movable
{
    public static class Functions
    {
        public static List<string> args = new List<string>();

        /// <summary>
        /// The m command. Moves every file from a folder to another given the source at  args[0] and the dest at args[1] 
        /// </summary>
        /// <returns></returns>
        public static bool m()
        {
            string source = args.ElementAt(0);
            string dest = args.ElementAt(1);
            Console.WriteLine("Deleting");
            foreach (string fl in Directory.GetFiles(dest))
            {
                if (File.Exists(fl))
                {
                    File.Delete(fl);
                    Console.WriteLine("Deleted " + fl);
                }
            }
            Console.WriteLine("Finished deleting");
            Console.WriteLine("Copying");
            foreach (string fl in Directory.GetFiles(source))
            {

                FileInfo inf = new FileInfo(fl);
                File.Copy(fl, dest + "/" + inf.Name);
                Console.WriteLine("Copied " + fl);
            }
            Console.WriteLine("Finished copying");
            return true;
        }

        /// <summary>
        /// The help command, shows the whole list of commands and their text, if it exists. 
        /// </summary>
        /// <returns></returns>
        public static bool helpall()
        {
            foreach (KeyValuePair<string, Tuple<Func<bool>, string, FunctionParameters.Parameters, int, string>> com in Dictionaries.FunctionDict)
            {
                Console.WriteLine(com.Key + " - " + com.Value.Item2);
            }
            return true;
        }

        /// <summary>
        /// The quit command. Needs a Terminate function to bypass the dictionary that only accepts functions returning bools. 
        /// </summary>
        /// <returns></returns>
        public static bool quit()
        {
            Console.WriteLine("Bye...");
            Terminate();
            return true;
        }

        /// <summary>
        /// Backups folders / files to AWS Glacier given the path to the data in args[0]
        /// </summary>
        /// <returns></returns>
        public static bool backup()
        {
                string acc = System.IO.File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "/awssetup/acc.txt");
                string folder = System.IO.File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "/awssetup/fold.txt");
                Console.Write("");
                Process cmdprocess = new Process();
                ProcessStartInfo startinfo = new ProcessStartInfo();
                startinfo.FileName = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "/awssetup/com.bat";
                startinfo.Arguments = acc + " " + "\"" + args.ElementAt(0) + "\"" + " eu-central-1 backup/" + folder + "/" + DateTime.Now.ToString("dd:MM:yyyy").Replace("/", ":") + "/" + System.IO.Path.GetFullPath(args.ElementAt(0));
                cmdprocess.StartInfo = startinfo;
                cmdprocess.Start();
                return true;
           

        }

        public static bool fromBase64()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            if (args.Count > 0)
            {
                Console.WriteLine(System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(args.ElementAt(0))));
            }
            return true;
        }

        public static bool toBase64()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            if(args.Count > 0){
                Console.WriteLine(System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(args.ElementAt(0))));
            }
            return true;
        }

        public static bool help()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            if (args.Count > 0)
            {
                if (Dictionaries.FunctionDict.ContainsKey(args.ElementAt(0)))
                {
                    if (Dictionaries.FunctionDict[args.ElementAt(0)].Item3 == FunctionParameters.Parameters.Yes)
                    {
                        int i = 0;
                        Console.WriteLine(Dictionaries.FunctionDict[args.ElementAt(0)].Item2);
                        Console.WriteLine("Params : ");
                        foreach (string str in Dictionaries.FunctionDict[args.ElementAt(0)].Item5.Split(' '))
                        {
                            Console.WriteLine("    [" + i.ToString() + "] : " + str);
                        }
                    }
                    else
                    {
                        Console.WriteLine("This function accepts no parameters.");
                    }
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("The function doesn't exist");
                }
                return true;
            }
            else
            {
                Console.WriteLine(Dictionaries.FunctionDict["help"].Item2);
                Console.WriteLine("Params :\n    [0] : " + Dictionaries.FunctionDict["help"].Item4);
                return true;
            }
        }

        public static bool parseGladysLog()
        {
            if (args.Count >= 2)
            {
                if (File.Exists(args.ElementAt(0)))
                {
                    string content = File.ReadAllText(args.ElementAt(0));
                    List<string> lines = content.Replace("END", "#").Split('#').ToList<string>();
                    int successCount = 0;
                    int errorCount = 0;
                    int warningCount = 0;
                    int infoCount = 0;
                    foreach (string s in lines)
                    {
                        s.Trim();
                        if (s.Contains("SUCCESS"))
                        {
                            successCount++;
                        }
                        if (s.Contains("ERROR"))
                        {
                            errorCount++;
                        }
                        if (s.Contains("WARNING"))
                        {
                            warningCount++;
                        }
                        if (s.Contains("INFO"))
                        {
                            infoCount++;
                        }
                    }
                    Console.WriteLine("LINES : " + lines.Count);
                    Console.WriteLine("SUCCESS : " + successCount);
                    Console.WriteLine("ERROR :  " + errorCount);
                    Console.WriteLine("WARNING : " + warningCount);
                    Console.WriteLine("INFO : " + infoCount);
                    if (args.ElementAt(1).ToLowerInvariant() == "y")
                    {
                        var errs = from s in lines where s.Contains("ERROR") select s.Split('|')[2];
                        foreach (string s in errs)
                        {
                            Console.WriteLine(s);
                        }
                    }
                    
                }
            }
            return true;
        }

        /// <summary>
        /// Basically. 
        /// </summary>
        private static void Terminate()
        {
            Environment.Exit(0);
        }
    }
}
