﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace movable
{
    class FunctionAPI : IDisposable
    {
        /// <summary>
        /// Returns true if the functions needs parameters.
        /// </summary>
        /// <param name="fnc"></param>
        /// <returns></returns>
        public bool functionNeedsArgs(Tuple<Func<bool>, string, FunctionParameters.Parameters, int, string> Tuple)
        {
            if(Tuple.Item3 == FunctionParameters.Parameters.No)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void Dispose()
        {
            GC.Collect();
        }
    }
}
